This program provides a solution to the protein folding problem in the HP model in 2D. 
It was written by HASSAN NEHME.

Sources:

[1] Emanuele Giaquinta Laura Pozzi. An effective exact algorithm and a new upper boundfor the number of contacts in the hydrophobic-polar 2d-lattice model .
[2] William E. Hart Sorin C. Istrail. Fast protein folding in the hydrophobic-hydrophilicmodel within three-eighths of optimal .
[3] OpenGlut, examples : https://www.opengl.org/archives/resources/code/samples/glut_examples/examples/examples.html .

How to start the program?

If you just want the optimal score:
[1] : ./make
[2] : 
dist/./projet : If you want to enter the sequence on the terminal .
dist/./projet data/liste.txt : If you have the sequences in the file liste.txt .
dist/./projet data/liste.txt data/score.txt : If you have the sequences in the file.txt and want the score to be written in score.txt .

If you also want the best conformation to be shown:
[1] : ./make graph
[2] : 
dist/./projetgraph : If you want to enter the sequence on the terminal .
dist/./projetgraph data/liste.txt : If you have the sequences in the file liste.txt .
dist/./projetgraph data/liste.txt data/score.txt : If you have the sequences in the file.txt and want the score to be written in score.txt .

Note : when you call projetgraph followed by liste.txt ( dist/./projetgraph data/liste.txt OR dist/./projetgraph data/liste.txt data/score.txt) when each conformation is shown click " X "(close the window) to show the get the next conformation.
