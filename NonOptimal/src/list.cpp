#include "list.h"

List::List()
{
	numberNodes=0;
	alignedNodesXPos=true;
	head=NULL;
}

//Copy constructor.
List::List(List* other)
	:numberNodes(other->numberNodes)
{
	if(other->head==nullptr)
		head = nullptr;
		
	else
	{	
		numberNodes = other->numberNodes;
		alignedNodesXPos = other->alignedNodesXPos;
		Node* currentNode = other->head; 
		head = new Node(currentNode->getLetter());
		while(currentNode->getNext()!=nullptr)
		{	
			currentNode = currentNode->getNext();
			Node* newNode = new Node(currentNode->getLetter());
			newNode->setNext(head);
			head = newNode;
		}
	}
}

List::~List()
{
	while(!head){
		remove();
	}
}
Node* List::getHead() const
{
	return head;	
}

int List::getNumberNodes() const
{
	return numberNodes;
}

bool List::areAlignedXPos() const
{
	return alignedNodesXPos;
}

//To know if all symbols are aligned in the linked list or not, if they are we can use symetry to accelerate calculation.
void List::checkAlignmentXPos() 
{	;
	Letter headLetter = head->getLetter();
	Vector2 headPosition = headLetter.getPosition();
	Node* node = head->getNext();
	Letter nodeLetter = node->getLetter();
	Vector2 nodePosition = nodeLetter.getPosition();
	Vector2 AbsDeltaVector = headPosition-nodePosition;
	AbsDeltaVector.absolute();
	if(!((AbsDeltaVector)==Vector2(1,0)))
		alignedNodesXPos = false;
}

//To insert an element in the linked list. Once inserted it becomes the new "Head" of the list, it is important to check if the new insertion breaks the alignment; if it does then we have to 
//set our boolean "alignedNodesXPos" to false this is why we call the method "checkAlignmentXPos()" .
void List::insert(Letter letter)
{	
	Node* temp = head;
	Node* n = new Node(letter);
	n->setNext(head);
	head = n;
	numberNodes++;
	if(temp!=nullptr && alignedNodesXPos==true)
		checkAlignmentXPos();
}

//To remove the last element that was inserted in the linked list. Again if we remove an element we need to re-check for the alignment.
void List::remove()
{
	if(head!=NULL)
	{		
		Node* temp = head;
		head = head->getNext();
		delete temp; 
		numberNodes--;
		if(alignedNodesXPos==false)
			checkAlignmentXPos();		
	}
}

//To print the list.
void List::printList() const
{
	Node* temp=head;
	cout << "List with " << numberNodes << " elements" << endl;
	cout << "positions: " <<endl;
	while(temp!=NULL)
	{	
		Letter tempLetter = temp->getLetter();
		Vector2 tempVector = tempLetter.getPosition();
		int x = tempVector.getX();
		int y = tempVector.getY();
		cout<<"(" <<tempLetter.getSymbol() << ": " << "(" << x << " , " << y << ")" << ")" << endl;
		temp = temp->getNext();
	}
	cout << endl;
}


//For Drawing...
std::vector<char> List::createProteinVect() const
{	
	std::vector<char> proteinVect;
	Node* firstNode = head;
	Node* secondeNode = head->getNext();
	proteinVect.push_back(head->getLetter().getSymbol());
	while(secondeNode!=nullptr)
	{
		Letter firstLetter = firstNode->getLetter();
		Letter secondeLetter = secondeNode->getLetter();
		
		char AdjacencyType = secondeLetter.getAdjacencyType(firstLetter);
		proteinVect.push_back(AdjacencyType);
		
		firstNode = firstNode->getNext();
		secondeNode = secondeNode->getNext();
		
		proteinVect.push_back(firstNode->getLetter().getSymbol());
	}
	return proteinVect;
}
