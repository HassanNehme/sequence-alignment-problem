#include "map.h"

map::map()
{
	
}

//Create a table containing all (x,y) positions on the lattice.
map::map(unsigned int nbrIntPosInit) : nbrIntPos(nbrIntPosInit) 
{	
	nbrCols = 2*nbrIntPosInit+1;
	unsigned int totalElements = nbrCols*nbrCols;
	lattice = new char[totalElements];
	for(unsigned int i=0; i<totalElements; i++)
	{
		lattice[i] = ' ';
	}
}

//Lattice was constructed on the heap, we need to delete it.
map::~map()
{	
	delete[] lattice; 
}

//To get the maximum positive integer on the x-axis.
unsigned int map::getNbrIntPos() const
{
	return nbrIntPos;
}

unsigned int map::getNbrCols() const
{
	return nbrCols;
}

//Copy constructor.
map::map(const map& other) : nbrIntPos(other.nbrIntPos) , nbrCols(other.nbrCols)
{	
	unsigned int totalElements = nbrCols*nbrCols;
	char* lattice = new char[totalElements];
	for(unsigned int i=0; i<totalElements; i++)
	{
		lattice[i] = ' ';
	}
}

//If an (x,y) position is occupied at some point in the program we need to update our lattice.
void map::updateMap(int i, int j, char symbol) 
{
	*( lattice + nbrCols*(j+nbrIntPos)+(i+nbrIntPos) ) = symbol;
}

//To know what letter is occupying an (x,y) position ( 'H' or 'P' ) we create this operator.
char map::operator()(int i, int j) const
{	
	char symbol = *(lattice + nbrCols*(j+nbrIntPos)+(i+nbrIntPos));
	return symbol;
}

//To know if an (x,y) position is occupied or not.
bool map::isTaken(int i, int j) const 
{	
	char symbol = this->operator() (i,j);
	if(symbol == ' ')
		return false;
	return true;
}

//To print the map.
void map::printMap() const
{
	for(unsigned int i=0; i<nbrCols*nbrCols ; ++i)
	{	
		if(((i+1)%nbrCols) == 0)
		{	
			cout << "i+1/nbrCols" << i+1<< "," << nbrCols << endl;
			cout << endl;
		}
		cout<<*(lattice + i);
		
	}
}

