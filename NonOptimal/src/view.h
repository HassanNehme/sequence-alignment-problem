#include <iostream>
#include <vector> 
#include "Vector2.h"
using std::cout;
using std::endl;
using std::string;

//This class is a singleton, this is why for each method we have static one sharing the same name and takes the same arguments.
//A singleton was used to create only one window at once, when each conformation is shown the user has to click " x " (close the window) to get the next conformation.
//The static method is going to call the Get() method that will return the single object of the class and on this object it calls the wanted method.
//It can be better than opening so many windows.
class view{
private:
	int energy;
	std::vector<char> proteinVect;
	Vector2 drawingPosition;
	view();
	void bitmap_output(int x, int y,const char* string, void *font) const;
	void output(int x, int y, char c, void *font) const;
	void drawLine(char c) ;
	void draw(unsigned int i) ;
	void display() ;
	void reshape(int w, int h) const;
	void initialize(int argc, char **arg) ;
	void setProteinVect(std::vector<char>& other);
	void setDrawingPosition(const Vector2& other);
	void setEnergy(int energyValue) ;
	Vector2 getDrawingPosition() const;
	std::vector<char> getProteinVect() const;
	void glutMain();
public:
	view(view& other) = delete;
	static view& Get();
	static void Idisplay();
	static void Ireshape(int w, int h);
	static void Iinitialize(int argc, char **arg);
	static void IsetProteinVect(std::vector<char>& other);
	static std::vector<char> IgetProteinVect() ;
	static void IsetDrawingPosition(const Vector2& other);
	static Vector2 IgetDrawingPosition();
	static void IsetEnergy(int energyValue);
	static void IglutMain();
};
