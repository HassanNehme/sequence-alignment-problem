#include <time.h>
#include "dataProcessor.h"
#include "view.h"
#include <fstream>
#include <stdlib.h>
using namespace std;

vector<int> start(char* protein, const unsigned int& length)
{	
	vector<int> scoreAndTime;
	//Start timing
	time_t start, end;
	time(&start);
	
	List bestPath;
	int score;
	
	//If the length of the protein is more than 4 we need to calculate the score.
	if(length>=4)
	{
		//Square lattice limits
		int xylimit;
		if(length<25)
			xylimit = floor(sqrt(length));
		else
			xylimit = floor(sqrt(length))-1;
			
		//Threads
		//CommonToAllThreads
		Letter letter(Vector2(1,0), (char)(protein[1]));
		
		//FirstThread
		Letter letterOne(Vector2(2,0), (char)(protein[2]));
		dataProcessor* datProOne = new dataProcessor(protein,length,xylimit);
		datProOne->insert(letter);
		datProOne->insert(letterOne);
		
		//SecondThread
		Letter letterTwo(Vector2(1,1), (char)(protein[2]));
		dataProcessor* datProTwo = new dataProcessor(protein,length,xylimit);
		datProTwo->insert(letter);
		datProTwo->insert(letterTwo);
		
		unsigned int MimimumMissesIn = (unsigned int)(pow(3,length));
		dataProcessor::setMinimumMisses(MimimumMissesIn);
		
		
		vector<dataProcessor*> dataProcessors;
		dataProcessors.push_back(datProOne);
		dataProcessors.push_back(datProTwo);
		
		//Start threads
		for(dataProcessor* datPro : dataProcessors)
		{
			datPro->runThread();
		}
		
		//Wait threads
		for(dataProcessor* datPro : dataProcessors)
		{
			datPro->worker.join();
		}
		
		//Delete what was created on the heap
		for(dataProcessor* datPro : dataProcessors)
		{
			delete datPro;
		}
		
	//BestPath is static, because it is common to all dataProccessors.
	bestPath = dataProcessor::getBestPath();
	score = dataProcessor::getEnergy();
	
	//Once we delete all the "dataProcessors" it is important to set the scoreMax attribut of the class to 0 because it is static
	//Otherwise it is going to stay equal to the maximum score of the last sequence that was analysed.
	dataProcessor::setEnergy(0);
	
	}
	
	//If the length is less than 4, we are sure that the score is 0 because we need at least 4 characters to get a single H-H.
	else
	{
		List possiblePath;
		for(unsigned int i = 0; i < length ; ++i)
		{
			Letter letter(Vector2(i,0), (char)(protein[i]));
			possiblePath.insert(letter);
		}
		bestPath = possiblePath;
		score = 0;
	}
	
	//End timing
	time(&end);
	float t_exec = difftime(end, start);
	
	//View
	int a = 0;
	char* b[0];
	vector<char> proteinVect = bestPath.createProteinVect();
	auto& viewer = view::Get();
	viewer.IsetProteinVect(proteinVect);
	viewer.IsetEnergy(-score);
	Vector2 startingPoint(250,250);
	viewer.IsetDrawingPosition(startingPoint);
	viewer.Iinitialize(a, b);
	viewer.IglutMain();
	
	//ScoreAndTime is a vector containing the maximum score of the sequence and the time taken to calculate it.
	scoreAndTime = {score, (int)t_exec};
	
	return scoreAndTime;
}

int
main(int argc, char* argv[])
{	
	if(argc>3)
	{
		cout<<"Dear user: "<<endl;
		cout<<"=========="<<endl<<endl;
		cout<<"1/ projetgraph: Input protein sequence on terminal you will get the best score on terminal and you will see the best folding graphiclly"<<endl<<endl;
		cout<<"2/ projetgraph liste.txt: write in liste.txt your sequences, you will get the best scores for each one on terminal and you will see the best folding graphiclly"<<endl<<endl;
		cout<<"3/ projetgraph liste.txt score.txt: exactly like the last one but the output will be in score.txt and you will see the best folding graphiclly "<<endl<<endl;
	
		_Exit(1);
	}
	
	//number of CPU cores.
	int nbrThreads = thread::hardware_concurrency();
	cout<< "program is going to run with " << nbrThreads << " threads." << endl;
	
	char* protein = new char;
	unsigned int length;
	//Get sequence from terminal
	if(argc==1)
	{
		//Receive sequence from user.
		cout << "Give me a protein in the HP model : ";
		cin >> protein;
		length = strlen(protein);
		
		if(cin.fail())
		{	
			cout<< " You have to enter a string representing a protein in the HP model!" << endl;
			delete protein;
			 _Exit(1);
			
		}
		for( unsigned int i=0 ; i<length; ++i )
		{
			if(protein[i] != 'H')
				if(protein[i] != 'P')
				{	
					cout<< " You have to enter a string representing a protein in the HP model!" << endl;
					delete protein;
					 _Exit(1);
				}
		}
		vector<int> scoreAndTime = start(protein, length);
		cout << protein << ":  "<< scoreAndTime[0] << " , "<< " execution time: "<< scoreAndTime[1] << " second(s)" <<endl;
	}	
	//Get sequence from text file.
	else if( argc == 2 || argc == 3)
	{
		vector<int> scoreAndTime;
		vector<string> sequences;
		ifstream file;
		file.open(argv[1]);
		string line;
		if (!file.is_open())
		{
			cout<<"Error while trying to open "<< argv[1] <<endl;
			_Exit(1);
		}
		while (getline(file, line) && line.size() != 0)
		{
			sequences.push_back(line);
		}
		if(argc == 2)
		{
			for(string s : sequences)
			{
				protein = &s[0];
				length = strlen(protein);
				scoreAndTime = start(protein, length);
				cout << protein << ":  "<< scoreAndTime[0] << " , "<< " execution time: "<< scoreAndTime[1] << " second(s)" <<endl;
			}
		}
		//Get sequence from text file and write results in another text file.
		else
		{
		  ofstream outFile (argv[2]);
		  if (!outFile.is_open())
		  {
			 cout<<"Error while trying to open or creating " << argv[2] <<endl;
			 _Exit(1);
		  }
		  
		  for(string s : sequences)
		  {
			 protein = &s[0];
			 length = strlen(protein);
			 scoreAndTime = start(protein, length);
			 outFile << s << " : " << scoreAndTime[0] << " , " << " execution time: "<< scoreAndTime[1] << " second(s)" << ".\n";
		  }
		    outFile.close();
		}
	}
	_Exit(0);
}
