#ifndef VECTOR2_H
#define VECTOR2_H 
#include <iostream> 
using std::cout;
using std::endl;
using std::string;

class Vector2{
	int x;
	int y;
public:
	Vector2(int xInit=0, int yInit=0);
	~Vector2();
	int getX() const;
	int getY() const;
	void setX(int newX);
	void setY(int newY);
	void addX(int relativeX);
	void addY(int relativeY);
	void absolute();
	Vector2 operator-(const Vector2& other) const;
	bool operator==(const Vector2& other) const;
};

#endif
