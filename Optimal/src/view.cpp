#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <GL/freeglut.h>
#include <stdlib.h>
#include "view.h"

#define spaceLength 10 
#define lineLength 10
#define xydifference 5

view::view()
{

}

void view::bitmap_output(int x, int y, const char* string, void *font) const
{
  int len, i;

  glRasterPos2f(x, y);
  len = (int) strlen(string);
  for (i = 0; i < len; i++)
  {
    glutBitmapCharacter(font, string[i]);
  }
}

void view::output(int x, int y, char c, void *font) const
{
  int initc = (int)c;
  glRasterPos2f(x, y);
  glutBitmapCharacter(font, initc);
}

void view::drawLine(char c) 
{
	glColor3f(0, 0.5, 0);
    glPointSize(2);
    int x = drawingPosition.getX();
    int y = drawingPosition.getY();
	switch(c)
	{
		case 'U':
			glBegin(GL_LINES);
				glVertex2f(x,y);
				glVertex2f(x,y+lineLength);
		    glEnd();
		    drawingPosition.addY(lineLength);
		    drawingPosition.addX(-xydifference);
		  break;
		case 'D':
		    glBegin(GL_LINES);
				glVertex2f(x,y);
				glVertex2f(x,y-lineLength);
		    glEnd();
		    drawingPosition.addY(-lineLength);
		    drawingPosition.addY(-spaceLength);
		    drawingPosition.addX(-xydifference);
		  break;
		case 'L':
			glBegin(GL_LINES);
				glVertex2f(x,y);
				glVertex2f(x-lineLength,y);
		    glEnd();
		    drawingPosition.addX(-lineLength);
		    drawingPosition.addX(-spaceLength);
		    drawingPosition.addY(-xydifference);
			break;
		case 'R':
			glBegin(GL_LINES);
				glVertex2f(x,y);
				glVertex2f(x+lineLength,y);
		    glEnd();
		    drawingPosition.addX(+lineLength);
		    drawingPosition.addY(-xydifference);
			break;
	}
}

void view::draw(unsigned int i) 
{	
	if(i==0)
	{
		int x0 = drawingPosition.getX();
		int y0 = drawingPosition.getY();
		output(x0, y0,proteinVect[0],GLUT_BITMAP_9_BY_15);
		return;
	}
	
	char arrowDir = proteinVect[i-1];
	switch(arrowDir)
	{
		case 'U':	
			drawingPosition.addX(xydifference);
			drawingPosition.addY(spaceLength);
			drawLine('U');
			break;
		case 'D':
			drawingPosition.addX(xydifference);
			drawLine('D');
			break;
		case 'L':	
			drawingPosition.addY(xydifference);
			drawLine('L');
			break;
		case 'R':	
			drawingPosition.addX(spaceLength);
			drawingPosition.addY(xydifference);
			drawLine('R');
			break;
	}
	
	int x = drawingPosition.getX();
	int y = drawingPosition.getY();
	output(x, y,proteinVect[i],GLUT_BITMAP_9_BY_15);

	if(i == proteinVect.size()-1)
	{	
		string text = "Energy value: ";
		string str_energy = std::to_string(energy);
		string out = text + str_energy;
		char* out_message = &*out.begin();
		bitmap_output(90,30,out_message,GLUT_BITMAP_9_BY_15);
	}
}


void view::display() 
{
  glClear(GL_COLOR_BUFFER_BIT);
  for(unsigned int i = 0; i < proteinVect.size(); i = i+2)
	draw(i);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glFlush();
}

void view::reshape(int w, int h) const
{
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, w, 0, h);
  glMatrixMode(GL_MODELVIEW);
}

void view::initialize(int argc, char **argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(400, 400);
  glutCreateWindow("Protein's best folding: ");
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glColor3f(0.0, 0.5, 0.0);
  glLineWidth(3.0);
  glutDisplayFunc(Idisplay);
  glutReshapeFunc(Ireshape);
  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);             /* ANSI C requires main to return int. */
}

std::vector<char> view::getProteinVect() const
{
	return proteinVect;
}

void view::setDrawingPosition(const Vector2& other) 
{
	drawingPosition = other;
}

void view::setEnergy(int energyValue)
{
	energy = energyValue;
}
		
void view::setProteinVect(std::vector<char>& other) 
{
	proteinVect = other;
}
		
Vector2 view::getDrawingPosition() const
{
	return drawingPosition;
}

void view::glutMain()
{
	glutMainLoop();	
}

//Public
view& view::Get()
{
	static view viewInstance;
	return viewInstance;
}

void view::IsetProteinVect(std::vector<char>& other)
{
	Get().setProteinVect(other);
}

std::vector<char> view::IgetProteinVect()
{
	return Get().getProteinVect();
}

void view::Idisplay()
{
	 Get().display();
}

void view::Ireshape(int w, int h)
{
	Get().reshape(w, h);
}


void view::Iinitialize(int argc, char **arg)
{
	return Get().initialize(argc, arg);
}

Vector2 view::IgetDrawingPosition()
{
	return Get().getDrawingPosition();	
}


void view::IsetDrawingPosition(const Vector2& other)
{
	Get().setDrawingPosition(other);	
}

void view::IsetEnergy(int energyValue)
{
	Get().setEnergy(energyValue);	
}

void view::IglutMain()
{
	Get().glutMain();
}
