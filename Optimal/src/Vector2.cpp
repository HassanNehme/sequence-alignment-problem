#include "Vector2.h"


Vector2::Vector2(int xInit, int yInit) : x(xInit) , y(yInit)
{

}

Vector2::~Vector2()
{

}


int Vector2::getX() const
{
	return x;
}

int Vector2::getY() const
{
	return y;
}

void Vector2::setX(int newX)
{
	x=newX;
}

void Vector2::setY(int newY)
{
	y=newY;
}

void Vector2::addX(int relativeX)
{
	x+=relativeX;
}

void Vector2::addY(int relativeY)
{
	y+=relativeY;
}

//To get the difference between two Vector2 objects we have this "-" operator.
Vector2 Vector2::operator-(const Vector2& other) const 
{
	return Vector2(x-other.x, y-other.y);
	
}

//To know if two Vector2 objects are equal we have this "=" operator.
bool Vector2::operator==(const Vector2& other) const
{
	if(x==other.x && y==other.y)
		return true;
	else
		return false;
}


void Vector2::absolute()
{
	x = abs(x);
	y = abs(y);
}




