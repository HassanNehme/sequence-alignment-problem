#include <thread>
#include <mutex>
#include <iterator>
#include "map.h"
#include "list.h"
#include <cstring>
#include <cmath>
using std::vector;

class dataProcessor {
private:
	static List bestPath;
	static unsigned int scoreMax;
	static unsigned int minimumMisses;
	static std::mutex mu;
	
	map* mapObj;
	List* path;
	char* protein;
	
	unsigned int length;
	int xylimit;
	unsigned int k;
	unsigned int TotalMisses;
	unsigned int scoreTot;
	
	bool isValidPosition(const Vector2& potentialPosition) const;
	void getPossiblePositions(vector<Vector2>& possiblePositions) const;
	int AdjacentPositionsAndScore() const;	
	int initializeScoreCalculation() const;
	void getBestScore();
public:
	std::thread worker;
	dataProcessor(char* proteinInit, unsigned int lengthInit, int xylimit, unsigned int kInit = 0, unsigned int TotalMissesInit = 0, unsigned int scoreTotInit = 0);
	~dataProcessor();
	static void setMinimumMisses(unsigned int& newMinimumMisses);
	void insert(Letter letter);
	void runThread() ;
	void insertLetters(std::vector<Letter>& letters);
	static List getBestPath();
	static int getEnergy();
	static void setEnergy(unsigned int newScoreMax);
	
};
