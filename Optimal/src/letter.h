#include <string>
#include <cstdlib> 
#include "Vector2.h"

class Letter {
	Vector2 position;
	char symbol;
public:
	Letter(Vector2 positionInit,char symbolInit=' ');
	~Letter();
	char getSymbol() const;
	Vector2 getPosition() const;
	void setSymbol(char newSymbol);
	void setPosition(Vector2 newPosition);
	bool isAdjacentTo(const Letter& other) const;
	char getAdjacencyType(const Letter& other) const;
};
