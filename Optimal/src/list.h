#include "node.h"
#include <vector>
using std::cout;
using std::endl;

class List
{
private:
	int numberNodes;
	bool alignedNodesXPos;
	Node* head;
public:
	List();
	~List();
	List(List* other);
	
	Node* getHead() const;
	int getNumberNodes() const;
	bool areAlignedXPos() const;
	void checkAlignmentXPos() ;
	void insert(Letter letter);
	void remove();
	void printList() const;
	std::vector<char> createProteinVect() const;
};
