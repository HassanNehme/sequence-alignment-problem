#include "letter.h"

Letter::Letter(Vector2 positionInit, char symbolInit) : position(positionInit) , symbol(symbolInit)
{
	
}

Letter::~Letter()
{
	
}

char Letter::getSymbol() const
{
	return symbol;
}

void Letter::setSymbol(char newSymbol)
{
	symbol=newSymbol;
}

Vector2 Letter::getPosition() const
{
	return position;
}

void Letter::setPosition(Vector2 newPosition)
{
	position=newPosition;
}

//To know if the letter is adjacent to another letter, meaning that the difference between the two positions in absolute value gives the vector (0,1) or (1,0).
bool Letter::isAdjacentTo(const Letter& other) const
{	
	Vector2 position2 = other.getPosition();
	Vector2 deltaVector = position-position2;
	deltaVector.absolute();
	
	if(symbol == other.getSymbol())
	{
		if(deltaVector==Vector2(1,0) || deltaVector==Vector2(0,1))
			return true;
		else
			return false;
	}
	return false;
}

//To get the type of adjacency. the "other" letter can be above, bellow, on the right or on the left.
char Letter::getAdjacencyType(const Letter& other) const
{
	Vector2 position2 = other.getPosition();
	Vector2 deltaVector = position-position2;
	
	char AdjacencyType = ' ';
	
	if(deltaVector==Vector2(0,1))
		AdjacencyType = 'U';
	else if(deltaVector==Vector2(0,-1))
		AdjacencyType = 'D';
	else if(deltaVector==Vector2(-1,0))
		AdjacencyType = 'L';
	else
		AdjacencyType = 'R';
	
	return AdjacencyType;
}
