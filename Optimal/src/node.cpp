#include "node.h"

Node::Node() : letter(Letter(Vector2(0,0),' ')), next(NULL){}

Node::Node(Letter letterInit) : letter(letterInit), next(NULL){}

Node* Node::getNext() const
{
	return next;
}

Letter Node::getLetter() const
{
	return letter;
}

void Node::setNext(Node* n)
{	
	next=n;
}

