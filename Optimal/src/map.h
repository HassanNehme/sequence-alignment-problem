#include <iostream>
using std::cout;
using std::endl;

class map {
	private :
		unsigned int nbrIntPos;
		unsigned int nbrCols;
		char* lattice;
	public :
		map();
		map(unsigned int nbrIntPosInit); 
		~map(); 
		map(const map& other);
		unsigned int getNbrIntPos() const;
		unsigned int getNbrCols() const;
		void updateMap(int i, int j, char symbol); //retourne 1 si la position (i,j) est occupée; 0 sinon.
		char operator()(int i, int j) const;
		bool isTaken(int i, int j) const;
		void printMap() const;
};

//one more method to put true or false at a certain position


