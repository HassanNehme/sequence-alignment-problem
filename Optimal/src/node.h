#include "letter.h"
using std::string;

class Node
{
private:
	Letter letter;
	Node* next;
public:
	Node();
	Node(Letter letter);
	Node* getNext() const;
	Letter getLetter() const;
	void setNext(Node* n);
};
