#include "dataProcessor.h"

dataProcessor::dataProcessor(char* proteinInit, unsigned int lengthInit,  int xylimitInit, unsigned int kInit, unsigned int TotalMissesInit, unsigned int scoreTotInit)
: protein(proteinInit), length(lengthInit),  xylimit(xylimitInit), k(kInit), TotalMisses(TotalMissesInit), scoreTot(scoreTotInit)
{
	path = new List();
	mapObj = new map(length-1);
	Letter origin(Vector2(0,0),(char)(proteinInit[0]));
	insert(origin);
}
	
List dataProcessor::bestPath;
unsigned int dataProcessor::scoreMax = 0;
std::mutex dataProcessor::mu;
unsigned int dataProcessor::minimumMisses;

//The only attributs that were created on the heap are path and mapObj, we delete them.
dataProcessor::~dataProcessor()
{
	delete path;
	delete mapObj;
};

//At the begininning of the program we need to set "minimumScore" to a certain value it has to be "infinity" or in other words "A big number" or simply an upper bound of the problem.
//we used an upper bound.
void dataProcessor::setMinimumMisses(unsigned int& newMinimumMisses)
{
	minimumMisses = newMinimumMisses;
}

//To insert a symbol in the linked list. When we insert an element we have to update the lattice too, indicating that a certain position (x,y) is now occupied and we increment k to get to the next
//element of the sequence.
void dataProcessor::insert(Letter letter) 
{
	path->insert(letter);
	mapObj->updateMap(letter.getPosition().getX(),letter.getPosition().getY(),letter.getSymbol());
	if(path->getNumberNodes() >= 4 && letter.getSymbol() == 'H')
	{	
		scoreTot += AdjacentPositionsAndScore();
		TotalMisses -= scoreTot;
	}
	k++;
}

//To know if the chosen position on the lattice is occupied or not.
bool dataProcessor::isValidPosition(const Vector2& potentialPosition) const
{	
	int x = potentialPosition.getX();
	int y = potentialPosition.getY();
	if( !(mapObj->isTaken(x,y)) && x<=xylimit  && y<=xylimit && x>=-xylimit && y>=-xylimit)
			return true;
	return false;
}

//To get the "3" or less possible positions on the lattice for a character.
void dataProcessor::getPossiblePositions(vector<Vector2>& possiblePositions) const //DONT TOUCH 
{	
	Node* head = path->getHead();
	Letter letter = head->getLetter();
	Vector2 potentialPosition = letter.getPosition();
	
	int i;
	unsigned int j = 0;
	int tabX[]={1,-1};
	const unsigned int sizeX = 2;
	for(i = tabX[0]; j < sizeX; i = tabX[j])
	{
		potentialPosition.addX(i);
		if(isValidPosition(potentialPosition))
			possiblePositions.push_back(potentialPosition);
		potentialPosition.addX(-i);
		j++;
	}
	
	j=0;
	int* tabY = new int[2];
	tabY[0] = -1; tabY[1] = 1;
	unsigned int sizeY = 2;
	if(path->areAlignedXPos())
	{
		tabY = new int[1];
		tabY[0] =  1;
		sizeY = 1;
	}
	for (i = tabY[0]; j < sizeY; i = tabY[j])
	{	
		potentialPosition.addY(i);
		if(isValidPosition(potentialPosition))
			possiblePositions.push_back(potentialPosition);
		potentialPosition.addY(-i);
		j++;
	}
	delete tabY;
}

//To get the contribution to the score of the last insertion in the linked list.
int dataProcessor::AdjacentPositionsAndScore() const
{	
	Node* head = path->getHead();
	Letter letter = head->getLetter();
	Vector2 position = letter.getPosition();
	
	Node* temp = head->getNext();
	Letter tempLetter = temp->getLetter();
	Vector2 tempPosition = tempLetter.getPosition();
	
	int score = 0;
	position.addX(1);
	char symbol = mapObj->operator ()(position.getX(), position.getY());
	if(symbol == 'H' && !(position==tempPosition))
		score+=1;
	position.addX(-1);
	position.addX(-1);
	symbol = mapObj->operator ()(position.getX(), position.getY());
	if(symbol == 'H' && !(position==tempPosition))
		score+=1;
	position.addX(+1);
	
	position.addY(1);
	symbol = mapObj->operator ()(position.getX(), position.getY());
	if(symbol == 'H' && !(position==tempPosition))
	{	
		score+=1;
	}
	position.addY(-1);
	
	position.addY(-1);
	symbol = mapObj->operator ()(position.getX(), position.getY());
	if(symbol == 'H' && !(position==tempPosition))	
		score+=1;
	position.addY(+1);
	
	return score;
} 

//To initialize the calculation of the contribution to the score of the last insertion.
int dataProcessor::initializeScoreCalculation() const
{	
	int retval = 0;
	retval = AdjacentPositionsAndScore();
	return retval;
}

//Algorithm
void dataProcessor::getBestScore()
{		
	if(k==length)
	{	
		mu.lock();
		if(scoreTot >= scoreMax)
		{	
			scoreMax = scoreTot;
			minimumMisses = TotalMisses;
			bestPath = path;
		}
		mu.unlock();
	}
	else
	{	
		char symbol = (char)protein[k];
		vector<Vector2> possiblePositions;
		getPossiblePositions(possiblePositions);
		auto itr = possiblePositions.begin();
		for(; itr<possiblePositions.end();itr++)
		{	
			int energy = 0;
			Letter newLetter(*itr,symbol);
			path->insert(newLetter);
			mapObj->updateMap( (*itr).getX(), (*itr).getY(), (char)protein[k]);
			if(symbol == 'H')
			{
				energy = initializeScoreCalculation();
				scoreTot += energy;
				TotalMisses += 3-energy;
			}
			mu.lock();
			unsigned int minimumMissesCo = minimumMisses;
			mu.unlock();
			if(TotalMisses < minimumMissesCo) //BranchAndBound
			{	
				k++;
				getBestScore(); 
				k--;
			}
			path->remove();
			mapObj->updateMap( (*itr).getX(), (*itr).getY(), ' ');
			if(symbol == 'H')
			{
				scoreTot -= energy;
				TotalMisses -= 3-energy;
			}
		}
	}
}

void dataProcessor::runThread() 
{
	worker = std::thread(&dataProcessor::getBestScore, this);	
}

//To insert a symbol in the linked list.
void dataProcessor::insertLetters(std::vector<Letter>& letters)
{	
	for(Letter& l: letters)
		insert(l);
}

//BestPath is static, it is common to all dataProcessors objects.
List dataProcessor::getBestPath() 
{
	return bestPath;
}

//ScoreMax is static, it is common to all dataProcessors objects.
int dataProcessor::getEnergy() 
{
	return scoreMax;
}

void dataProcessor::setEnergy(unsigned int newScoreMax)
{
	scoreMax = newScoreMax;
}
